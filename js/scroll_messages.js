jQuery(function($){
    var messages = [
        "Hello!",
        "This is a website!",
        "You are now going to be redirected.",
        "Are you ready?",
        "Then that's it!",
        "You're now being redirected..."
    ];

    var msgId = 0;

    (function nextMsg(){
        jQuery('.scroll_messages h1').html(messages[msgId++] || messages[msgId = 0, msgId++]).fadeIn(500).delay(6000).fadeOut(500, nextMsg);
    })();
});