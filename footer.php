<div class="container">
	<footer>
		<div class="col-md-8">
			<span class="address">
				Av. Evandro Lins e Silva 840, Sala 319, Barra da Tijuca<br />
				Rio de Janeiro, Brasil
			</span>

			<span class="phone">
				+55 21 2480-2405 | +55 21 3079-3451 | +55 21 98206-9998
			</span>

			<span class="email">
				biolotus@biolotus.com
			</span>
		</div>

		<div class="col-md-4 text-right">
			<p class="copyright">&copy; Copyright 2016. Todos os direitos reservados.</p>

			<a href="#" class="socials">
				<i class="fa fa-rss-square" aria-hidden="true"></i>
			</a>
			
			<a href="#" class="socials">
				<i class="fa fa-linkedin-square" aria-hidden="true"></i>
			</a>

			<a href="#" class="socials">
				<i class="fa fa-facebook-square" aria-hidden="true"></i>
			</a>

			<a href="#" class="socials">
				<i class="fa fa-twitter-square" aria-hidden="true"></i>
			</a>
		</div>
	</footer>
</div>
		
<?php wp_footer(); ?>
</body>
</html>
