<?php
get_header();
while(have_posts()): the_post(); 

$parent_title = get_the_title($post->post_parent);
$banner_page = get_field('banner');

?>



<div class="container">
	<div class="banner_page" style="background: url(<?=$banner_page?>) top center;">
		<div class="col-md-12">
			<h2><?php echo $parent_title; ?></h2>
			<h1><?php the_title(); ?></h1>
		</div>
	</div>
</div>

<div class="container">
	<div class="col-md-12 conteudo">
		<?php the_content(); ?>
	</div>
</div>

<?php endwhile;?>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
