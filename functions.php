<?php

add_filter('show_admin_bar', '__return_false');
require_once('inc/wp_bootstrap_navwalker.php');
require_once('inc/plugins_include.php');

if(!function_exists( 'biolotus_setup')) :
function biolotus_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Twenty Sixteen, use a find and replace
	 * to change 'twentysixteen' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'biolotus', get_template_directory() . '/languages' );

	
}
endif; // biolotus_setup
add_action( 'after_setup_theme', 'biolotus_setup' );

add_theme_support('post-thumbnails');

/**
 * Enqueues scripts and styles.
 */
function biolotus_scripts() {

	wp_enqueue_style('bootstrap', get_template_directory_uri().'/css/bootstrap.min.css', false);
	wp_enqueue_style('fontawesome', get_template_directory_uri().'/css/font-awesome.min.css', false);
	wp_enqueue_style('biolotus-css', get_stylesheet_uri());
	wp_enqueue_style('slideshow', get_template_directory_uri().'/css/style1.css', false);
	
	wp_enqueue_script('bootstrap-js', get_template_directory_uri().'/js/bootstrap.min.js', array('jquery'));
	wp_enqueue_script('scroll-messages', get_template_directory_uri().'/js/scroll_messages.js', array('jquery'));
	wp_enqueue_script('slideshow', get_template_directory_uri().'/js/modernizr.custom.86080.js', array());

}
add_action('wp_enqueue_scripts', 'biolotus_scripts');

function getUrlInTargetLanguage($targetLang){
    global $qtranslate_slug;
    return $qtranslate_slug->get_current_url($targetLang);
}

register_nav_menus(array(
    'primary' => 'Menu Principal',
));

