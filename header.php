<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!--
	*  Desenvolvido por: Codemax Sistemas - http://www.codemax.com.br
	*  
	*  Skype: lucas.codemax
	*  E-mail: lucas.codemax@gmail.com
	-->

	<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700,900,400italic' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Playfair+Display:700,900|Fira+Sans:400,400italic' rel='stylesheet' type='text/css'>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<head profile="http://gmpg.org/xfn/11">
	<title><?php
        if(is_single()){
        	single_post_title();
        }
        
        elseif(is_home() || is_front_page()){
        	bloginfo('name'); print ' | '; bloginfo('description');
        }
        
        elseif( is_search() ) { bloginfo('name'); print ' | Resultado da busca por ' . wp_specialchars($s);}
        elseif( is_404() ) { bloginfo('name'); print ' | Nada encontrado'; }
        else { bloginfo('name'); wp_title('|');}
    ?></title>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<header>
		<div class="bar_top">
			<div class="container">
				<!-- Se a pagina não for a Index -->
				<div class="col-md-6 noPadding-left">
				<?php if(!is_home() || !is_front_page()):?>
					<a href="<?php bloginfo('url');?>">
						<img src="<?=get_template_directory_uri()?>/images/logo.png" class="logo img-responsive" />
					</a>
				<?php endif; ?>
				</div>
				
				<div class="col-md-6 noPadding-right text-right linguas">
					<?php
						qtranxf_generateLanguageSelectCode('short');
					?>
				</div>
			</div>
		</div>

		<nav class="navbar navbar-default">
		  	<div class="container">
		    
		    	<div class="navbar-header">
		      		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        		<span class="sr-only">Toggle navigation</span>
		        		<span class="icon-bar"></span>
		        		<span class="icon-bar"></span>
		        		<span class="icon-bar"></span>
		      		</button>
		    	</div>

		    	<?php
		            wp_nav_menu( array(
		                'menu'              => 'primary',
		                'theme_location'    => 'primary',
		                'depth'             => 2,
		                'container'         => 'div',
		                'container_class'   => 'collapse navbar-collapse',
		        		'container_id'      => 'bs-example-navbar-collapse-1',
		                'menu_class'        => 'nav navbar-nav navbar-right',
		                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
		                'walker'            => new wp_bootstrap_navwalker())
		            );
		        ?>
		  	</div><!-- /.container-fluid -->
		</nav>
	</header>