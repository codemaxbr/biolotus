<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<div class="scroll_messages">
	<div class="container">
		<div class="col-md-6">
			<a class="navbar-brand" href="#">
      			<img src="<?=get_template_directory_uri()?>/images/logo_color.png" />
      		</a>
		</div>
		<div class="col-md-6 pull-right text-right">
			<h1>Willing to dare the impossible, we work for a better and healthier world!</h1>
		</div>
	</div>
</div>


<ul class="cb-slideshow">
    <li><span>Image 01</span><div></div></li>
    <li><span>Image 02</span><div></div></li>
    <li><span>Image 03</span><div></div></li>
    <li><span>Image 04</span><div></div></li>
    <li><span>Image 05</span><div></div></li>
    <li><span>Image 06</span><div></div></li>
</ul>
